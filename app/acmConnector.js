"use strict";
var utpApi = require('./utpApi');
var log = require('electron-log');
var request = require('request');
var config = require('./config');
var utils = require('./utils');
var async = require('async');
var fs = require('fs');

utpApi.utpApiPort = config.appvance.port;

var self = module.exports = {
  checkLogins: function() {
    utils.onceAppvanceUp(true, function() {
      utpApi.admin.loggin('localhost', 'appvance', 'appvance', function(err) {
        if (err) {
          log.error(err);
        } else {
          utpApi.admin.getLogInHistory('localhost', function(err, data) {
            if (err) {
              log.error(err);
            } else if (data) {

              var loginlines = data.split('\n');
              loginlines = loginlines.slice(Math.max(loginlines.length - 20, 1));
              var loginArray = [];
              for (var line in loginlines) {
                loginArray.push(loginlines[line].match(/(.+),(.+),(.+)/));
              }

              // Remove headers
              var cleanArray = [];
              for (var item in loginArray) {
                if (loginArray[item][1] == "appvance") {
                  // log.info("Deleted: " + loginArray[item])
                  // loginArray.pop([item])
                } else {
                  if (loginArray[item] !== "" || loginArray[item] !== "appvance") {
                    // log.info("adding loginArray item to cleanArray")
                    cleanArray.push({
                      username: loginArray[item][1],
                      ip: loginArray[item][2],
                      date: loginArray[item][3]
                    });
                  }
                }
              }

              //Need to add part to reach out to acm to update login history
              self.updateLoginHistory(cleanArray);

            }
          });
        }
      });
    });
  },

  checkLicenseUsage: function() {
    utils.onceAppvanceUp(true, function() {
      utpApi.admin.loggin('localhost', 'appvance', 'appvance', function(err) {
        if(err){
          log.error(err);
        } else{
          utpApi.license.getLicenseTimeUsage('localhost', function(err, result) {
            if (err) {
              log.error(err);
            } else {
              // Usage Lines
              var usagelines = result.split('\n');
              var usageArray = [];
              for (var line in usagelines) {
                usageArray.push(usagelines[line].match(/"(.+?)",(.+),(.+)/));
              }
              // Remove headers
              usageArray.shift();
              var formattedUsageArray = [];
              for (var use in usageArray) {
                if (usageArray[use] !== null) {
                  formattedUsageArray.push({
                    'virtualUserMinutesSpent': (usageArray[use][2] - 0),
                    'scenarioFileName': usageArray[use][3],
                    'scenarioFileRunDate': usageArray[use][1]
                  });
                }
              }

              //reach out to acm to update usage
              self.updatelicenseUsage(formattedUsageArray);

            }
          });
        }
      });
    });
  },

  setLicense: function(license, callback) {
    log.debug("Attempting to set license");
    if (typeof callback !== 'function') {
      callback = log.error;
    }
    fs.writeFile(config.licenseFile, license, function(err) {
      if (err) {
        callback(err);
      } else {
        utils.onceAppvanceUp(false, function(err) {
          if (err) {
            log.error("Failed while waiting on appvance");
            callback(err);
          }
          if (!err) {
            utpApi.admin.loggin('localhost', 'appvance', 'appvance', function(err) {
              if (err) {
                callback(err);
              } else {
                log.debug("logged in to appvance");
                utpApi.license.setLicense('localhost', fs.createReadStream(config.licenseFile), function(err) {
                  if (err) {
                    log.error("Unable to call setLicense API");
                    callback(err);
                  } else {
                    log.warn("Set License");
                    callback(null);
                  }
                });
              }
            });
          }
        });
      }
    });
  },

  registerClient: function(companyName, userName, callback) {
    log.info("Trying to register client with company " + companyName + " and userName " + userName);

    var resource = '/trials/registerClient';
    var options = {
      json: {
        companyName: companyName,
        userName: userName,
        os: process.platform,
        clientId: config.uuid
      }
    };
    request.post(config.acm.url + resource, options, function(err, httpResponse, body) {
      // log.debug(body);
      if (err) {
        log.error("failed to get license from acm server");
        log.error(err);
        callback(err);
      } else if (body.result == "success") {
        log.warn("Registered client with acm");
        callback(null, body.license);
      } else {
        log.error("Unable to register client with acm");
        log.error(JSON.stringify(httpResponse));
        log.error(body);
        callback(new Error("Failed to register client"));
      }
    });
  },


  updateLicenseUsage: function(licenseUsage) {
    var resource = '/trials/updateLicenseUsage';
    var options = {
      json: {
        companyName: config.companyName,
        clientId: config.uuid,
        licenseUsage: licenseUsage
      }
    };
    request.post(config.acm.url + resource, options, function(err, httpResponse, body) {
      if (err) {
        log.error("failed to get license from acm server");
        log.error(err);
      }
    });
  },

  updateLoginHistory: function(loginHistory) {
    var resource = '/trials/updateLoginHistory';
    var options = {
      json: {
        companyName: config.companyName,
        clientId: config.uuid,
        loginHistory: loginHistory
      }
    };
    request.post(config.acm.url + resource, options, function(err, httpResponse, body) {
      if (err) {
        log.error("failed to get license from acm server");
        log.error(err);
      }
    });
  },

  getLicense: function(companyName, userName, callback) {
    var resource = '/trials/getLicense';
    var options = {
      form: {
        companyName: companyName,
        userName: userName
      }
    };
    request.post(config.acm.url + resource, options, function(err, httpResponse, body) {
      if (err) {
        log.error("failed to get license from acm server");
        log.error(err);
        callback(err);
      } else if (body.license) {
        callback(null, body.license);
      }
    });
  }
};

var serverUp = false;
async.until(function() {
    return serverUp;
  },
  function(callback) {
    setTimeout(function() {
      request.get(config.appvance.getUrl(), {
        timeout: 5000
      }, function(err, httpResponse, body) {
        if (err) {
          if (['ETIMEDOUT', 'ECONNREFUSED', 'ESOCKETTIMEDOUT'].includes(err.code)) {
            callback(null);
          } else {
            callback(err);
          }
        } else if (httpResponse.statusCode == 200) {
          serverUp = true;
          log.info("Appvance Server is Up");
          callback(null);
        } else {
          callback(null);
        }
      });
    }, 10000);
  },
  function(err) {
    if (err) {
      log.info("failed to reach new trial " + config.appvance.url);
      log.error(err);
    } else {
      async.forever(function(callback) {
          setTimeout(function() {
            async.parallel([
                self.checkLogins,
                self.checkLicenseUsage
              ],
              function(err, result) {
                if (err) {
                  log.error(err);
                } else {
                  log.info("updated usage");
                  callback();
                }
              }
            );
          }, 3600000);
        },
        function(err) {
          if (err) {
            log.error(err);
          }
        });
    }
  }
);