"use strict";
var request = require('request');
// var FormData = require('form-data');
var log = require('electron-log');

var self = module.exports = {
  baseUtpResource:'/AppvanceServer/rest/',
  utpApiPort: '8080',
  cookieJar: request.jar(),
  defaultTimeout: 15000,

  callUtpApi: function(server, resource, data, callback){
    if (!server) {
      throw Error("Please specify server in request");
    }

    var options = {
      jar: self.cookieJar,
      timeout: (typeof data.timeout === "undefined" ? self.defaultTimeout : data.timeout)
    };
    if (data) {

      if ("file" in data){
        options.formData = {};
        options.formData.upload = data.file;
        if("form" in data){
          for (var key in data.form){
            options.formData[key] = data.form[key];
          }
        }
      } else if ("form" in data){
        options.form = data.form;
      } else if ("json" in data){
        options.json = data.json;
      }

      if ("queryParams" in data){
        options.qs = data.queryParams;
      }
    }

    var finalUrl = 'http://'+server+':'+self.utpApiPort+self.baseUtpResource+''+resource;
    // console.log(finalUrl);
    // console.log(options);

    request.post(finalUrl, options, function(err,httpResponse,body){
      if (err){callback(err, null);}
      else if (httpResponse.statusCode == 200){
        callback(null, body);
      } else{
        callback(Error("Did not receive 200 http response. Received "+httpResponse+". Body: "+body), null);
      }
    });
  },

  admin: {
    loggin: function(server, username, password, callback){
      var resource = "admin/loggin";
      var options = {
        form:{
          username: username,
          password: password
        }
      };
      self.callUtpApi(server, resource, options, callback);
    },

    addUser: function(server, username, password, role, callback){
      var resource = "admin/addUser";
      var options = {
        form:{
          username: username,
          password: password,
          role: role
        }
      };
      self.callUtpApi(server, resource, options, callback);
    },

    // updateUser: function(server, password, callback){
    //   resource = "admin/updateUser"
    //   self.callUtpApi(server, resource, {form:{password:password}}, callback)
    // },
    
    changePassword: function(server, password, callback){
      var resource = "admin/changePassword";
      self.callUtpApi(server, resource, {form:{password:password}}, callback);
    },

    getLogInHistory: function(server, callback){
      var resource = "admin/getLogInHistory";
      self.callUtpApi(server, resource, null, callback);
    }

  },
  apps:{
    shutdown: function(server, callback){
      var resource = "apps/shutdown";
      self.callUtpApi(server, resource, null, callback);
    },
    reboot: function(server, callback){
      var resource = "apps/reboot";
      self.callUtpApi(server, resource, null, callback);
    }
  },

  file: {
    updateFile: function(server, path, fileName, file, callback){
      var resource = "file/updateFile";
      var options = {
        form:{
          filename: fileName
        },
        file: file,
        queryParams:{
          path: path
        }
      };
      self.callUtpApi(server, resource, options, callback);
    }
  },

  license: {
    getLicenseTimeUsage: function(server,callback){
      var resource = "license/getLicenseTimeUsage";
      self.callUtpApi(server, resource, null, callback);
    },

    getVersion: function(server,callback){
      var resource = "license/getVersion";
      self.callUtpApi(server,resource, null, callback);
    },

    getOutput: function(server,callback){
      var resource = "license/getOutput";
      self.callUtpApi(server,resource, null, callback);
    },

    setLicense: function(server, file, callback){
      var resource = "license/setLicense";
      self.callUtpApi(server, resource, {file: file}, callback);
    }
  },

  preferences: {
    saveResultsRepository: function(server, params, callback){
      var resource = "preferences/saveResultsRepository";
      var defaultParams = {
        "username" : "utp",
        "password" : "utp",
        "engine" : "MySQL",
        "url" : "jdbc:mysql://localhost:54322/utp",
        "database" : "utp",
        "maxActive" : "100",
        "maxIdle" : "30",
        "maxWait" : "1000",
        "abandonedTimeout" : "60",
        "autoCommit" : true,
        "logAvandoned" : true,
        "removeAvandoned" : true,
        "enabled" : true
      };


      for (var key in params){
        defaultParams[key] = params[key];
      }
      // log.debug(defaultParams);
      self.callUtpApi(server, resource, {json:defaultParams}, callback);
    },

    saveRepository: function(server, params, callback){
      var resource = "preferences/saveRepository";
      if (! ("name" in params)){
        throw Error("Proved a 'name' for repo in params.");
      } else if(!("url" in params)){
        throw Error("Proved a 'url' for repo in params.");
      } else if(!("username" in params)){
        throw Error("Proved a 'username' for repo in params.");
      } else if(!( "password" in params) || !("ssh" in params)){
        throw Error("Proved a 'password' or 'ssh' key for repo in params.");
      } else if(!("type" in params)){
        throw Error("Proved a 'type' for repo in params. either GIT or SVN");
      }

      if ("password" in params){
        params.auth = "Password";
      } else if ("ssh" in params){
        params.auth = "SSH";
      }

      self.callUtpApi(server, resource, {json:params}, callback);
    }
  }
};
