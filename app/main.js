"use strict";
// TODO: SetUp auto installer
// if(require('electron-squirrel-startup')) return;
// const autoUpdater = require('auto-updater');  

// autoUpdater.addListener("update-available", function(event) {  
// });
// autoUpdater.addListener("update-downloaded", function(event, releaseNotes, releaseName, releaseDate, updateURL) {  
// });
// autoUpdater.addListener("error", function(error) {  
// });
// autoUpdater.addListener("checking-for-update", function(event) {  
// });
// autoUpdater.addListener("update-not-available", function(event) {  
// });
// const os = require('os');  
// const feedURL = 'http://appvance.s3.amazonaws.com/updates/latest/win' + (os.arch() === 'x64' ? '64' : '32');  
// autoUpdater.setFeedURL(feedURL); 

const electron = require('electron');
const path = require('path');
var exec = require('child_process').exec;
var spawn = require('child_process').spawn;
var async = require('async');
// var request = require('request');
// var os = require('os');
var fs = require('fs');
var mysql = require('mysql');
var log = require('electron-log');

var logLevel = 'warn';
if(process.env.NODE_ENV === 'DEVELOPMENT'){
  logLevel = 'debug';
}
log.transports.file.level = logLevel;
log.transports.console.level = logLevel;
log.info("Log level: "+logLevel);

//Determine os specifics
var baseFilePath = '';
var javaExecutable = '';

if (process.platform == 'win32') {
  javaExecutable = 'java.exe';
  baseFilePath = path.normalize('resources');
} else if (process.platform == 'darwin') {
  javaExecutable = 'java';
  baseFilePath = path.resolve(path.join(__dirname, '../'));
} else {
  throw new Error("cannot determine os " + process.platform);
}

console.log(baseFilePath);
console.log(path.normalize(path.join(baseFilePath, 'appvance.log')));
log.transports.file.file = path.normalize(path.join(baseFilePath, 'appvance.log'));

//Import customer modules once logging has been established
var utpApi = require('./utpApi');
var config = require('./config');
var utils = require('./utils');
var acmConnector = require('./acmConnector');


// Module to control application life.
const app = electron.app;
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow;
const Menu = electron.Menu;
const Tray = electron.Tray;
// const dialog = app.dialog;

var dbConnection;
var stoppingProgram = false;

function maintainDbConnection(){
  dbConnection = mysql.createConnection(config.getDbConfig());

  dbConnection.connect(function(err){
    if(err){
      if(!stoppingProgram){
        log.debug("Error when connecting to db: ",err.code);
        setTimeout(maintainDbConnection, 2000);
      }
    } else{
      log.warn("Connected to db.");
    }
  });
  dbConnection.on('error', function(err){
    if (['PROTOCOL_CONNECTION_LOST',"ECONNREFUSED", "ECONNRESET", "ETIMEDOUT"].includes(err.code)){
      if(!stoppingProgram){
        maintainDbConnection();
      }
    } else {
      log.warn("mysql disconnected");
      throw err;
    }
  });
}



// Keep a global reference of the window objects, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
global.windows = [];
global.startWindow = null;
global.mainWindow = null;
var appvanceWindow;
var helpWindow;
var startUpBrowserWindow;
var setupBrowserWindow;
var advancedBrowserWindow;

//variable to designate that program is shutting down


var mysqlChild;
var appvanceChild;

function installMysql(callback){
  if (config.mysqlInitialized) {
    log.info("mysql already initialized");
    global.windows[0].webContents.send('current-proc', 'database already initialized');
    callback(null);
  } else {
    log.info("mysql not installed. Starting instalation");
    log.debug("Starting mysql install. command: "+config.getMysqlInitCommand());
    var child = exec(config.getMysqlInitCommand(), {}, function(err, stdout, stderr) {
      if (err) {
        log.error("Unable to initialize mysql");
        log.error("command: " + config.getMysqlInitCommand());
        log.error('stdout: ' + stdout);
        log.error('stderr: ' + stderr);
        callback(err);
      } else {
        config.mysqlInitialized = true;
        config.saveSystemSettings();
        log.info("Initialized/installed mysql");
        log.debug("stdout: " + stdout);
        callback(null);
      }
    });
    log.debug("Started mysql installation");
    global.windows[0].webContents.send('current-proc', "initializing database");
    child.stdout.on('data', function(data) {
      global.windows[0].webContents.send('current-proc', data.toString());
    });
  }
}

function ensureMysqlUp(callback){
  var ready = false;
  global.windows[0].webContents.send('current-proc', 'waiting for database to be available');
  async.until(function() {
      return ready;
    },
    function(callback) {
      if (stoppingProgram){
        callback(new Error("stopping check, shutting down"));
      }
      setTimeout(function() {
        log.debug("testing mysql connection");
        dbConnection.query("SELECT true;", function(err) {
          if (err) {
            if (["ECONNREFUSED", "ECONNRESET"].includes(err.code)) {
              log.info("mysql still not up.");
              callback(null);
            } else {
              callback(null);
            }
          } else {
            ready = true;
            log.info("mysql is up and accepting connections");
            callback(null);
          }
        });
      }, 5000);
    },
    function(err) {
      if (err) {
        callback(err);
      } else {
        global.windows[0].webContents.send('current-proc', 'database available');
        callback(null);
      }
    }
  );
}

function provisionMysql(callback){
  ensureMysqlUp(function(){
    global.windows[0].webContents.send('current-proc', 'starting database provisioning');
    var init_sql = '';
    async.series([
      function(callback) {
        fs.readFile(config.mysqlInitSqlFile, function(err, data) {
          if (err) {
            callback(err);
          } else {
            log.debug("read init sql script");
            init_sql = data.toString();
            callback(null);
          }
        });
      },
      function(callback) {
        dbConnection.query(init_sql, function(err) {
          if (err) {
            log.error("Failed running init sql");
            callback(err);
          } else {
            global.windows[0].webContents.send('current-proc', 'running init scripts');
            log.info("ran init sql script");
            callback(null);
          }
        });
      },
      function(callback) {
        var child = exec(config.getMysqlAppvanceSqlCommand(), function(err){
          if (err) {
            log.error("Unable to run appvance provision sql");
            log.error(err);
            callback(err);
          } else {
            global.windows[0].webContents.send('current-proc', 'provisioned database for appvance');
            log.info("appvance provision sql succesful");
            callback(null);
          }
          child.stdout.on('data', function(data) {
            global.windows[0].webContents.send('current-proc', data.toString());
          });
        });
      }
    ], function(err) {
      if (err) {
        if (!stoppingProgram){
          log.error("failed to initialize mysql");
          log.error(err);
          callback(err);
        }
      } else {
        log.info("Provisioned mysql for appvance");
        global.windows[0].webContents.send('current-proc', 'database fully provisioned');
        config.mysqlAppvanceInitialized = true;
        config.saveSystemSettings();
        callback(null);
      }
    });
  });
}

function startMysql() {
  log.info("checking whether mysql initialized/provisioned");
  global.windows[0].webContents.send('current-proc', 'verifying database');
  log.warn("Starting mysql");
  var options = {
    shell: true
  };
  mysqlChild = spawn(config.mysqlStartMainCommand, config.getMysqlStartMainCommandArgs(), options);
  mysqlChild.on('error', function(err){
    if (!stoppingProgram) {
      log.error("Mysql ran into an error");
      log.error("command: " + config.getMysqlStartCommand());
      throw err;
    }
  });
  mysqlChild.on('end', function(){
    if(!stoppingProgram){
      log.warn('mysql ended unexpectedly');
    }
  });
  global.windows[0].webContents.send('current-proc', 'starting database');

  mysqlChild.stdout.on('data', function(data) {
    config.mysqlCommandStdoutWriteSteam.write(data.toString());
    global.windows[0].webContents.send('current-proc', data.toString());
  });

  mysqlChild.stderr.on('data', function(data) {
    config.mysqlCommandStderrWriteSteam.write(data.toString());
  });
}


function startAppvanceProc() {
  //Only start appvance if it isn't already running
  if (!appvanceChild) {
    var options = {
      cwd: config.appvanceStartDir,
      shell: true
    };
    appvanceChild = spawn(config.appvanceMainCommand, config.getAppvanceMainCommandArgs(), options);


    appvanceChild.on('error', function(err){
      if (!stoppingProgram) {
        log.error("Appvance service ran into an error");
        log.error("command: " + config.getAppvanceCommand());
        throw err;
      }
    });

    global.windows[0].webContents.send('current-proc', 'starting main appvance service');

    appvanceChild.stdout.on('data', function(data) {
      config.appvanceCommandStdoutWriteSteam.write(data.toString());
      global.windows[0].webContents.send('current-proc', data.toString());
    });

    appvanceChild.stderr.on('data', function(data) {
      config.appvanceCommandStderrWriteSteam.write(data.toString());
    });
  }
}

function startAppvance() {
  //Verify Appvance is running, then load it.
  startAppvanceProc();
  utils.onceAppvanceUp(true, function(err) {
    if (err) {
      if (!stoppingProgram) {
        log.error("failed to start appvance");
        throw err;
      }

    } else {

      global.windows[0].webContents.send('current-proc', 'appvance process up, loading');
      log.warn("Appvance is up, serving " + config.appvance.getUrl());
        // and load the index.html of the app.
        // global.windows[0].loadURL(`file://${__dirname}/index.html`)
      var options = {
        width: 1400,
        height: 1000,
        webPreferences: {
          nodeIntegration: false
        },
        show: false
      };
      var appvanceWindow = new BrowserWindow(options);
        //This is to ensure that clicking on things that open new windows will keep it within electron window
      appvanceWindow.webContents.on('new-window', function(e, url) {
        log.debug("new url: "+url);
        e.preventDefault();
        appvanceWindow.loadURL(url);
      });
      appvanceWindow.webContents.on('will-navigate', function(e, url){
        log.debug("new url: "+url);
        if(url.indexOf('userguide') > -1 ){
          var contentBounds = global.windows[0].getContentBounds();
          options.x = contentBounds.x + 25;
          options.y = contentBounds.y + 25;
          e.preventDefault();
          var helpWindow = new BrowserWindow(options);
          helpWindow.loadURL(url);
          helpWindow.on('ready-to-show', function(){
            helpWindow.show();
            helpWindow.focus();
          });
        }
      });
      appvanceWindow.loadURL(config.appvance.getUrl());
      appvanceWindow.on('ready-to-show', function() {
        appvanceWindow.show();
        appvanceWindow.focus();
        // global.windows[0].destroy();
        global.windows[0] = appvanceWindow;
        appvanceWindow = null;
      });
      appvanceWindow.focus();
      if (config.isDevEnv){
        appvanceWindow.webContents.openDevTools();        
      }
      appvanceWindow.on('closed', function() {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should devare the corresponding element.
        appvanceWindow = null;
      });



      utpApi.admin.loggin(config.appvance.host, config.appvance.user, config.appvance.password, function(err) {
        if (err) {
          log.error(err);
        } else {
          log.info("Setting result repository");
          var params = {
            "username" : "utp",
            "password" : "utp",
            "engine" : "MySQL",
            "url" : "jdbc:mysql://localhost:54321/utp",
            "database" : "utp",
            "maxActive" : "100",
            "maxIdle" : "30",
            "maxWait" : "1000",
            "abandonedTimeout" : "60",
            "autoCommit" : true,
            "logAvandoned" : true,
            "removeAvandoned" : true,
            "enabled" : true
          };
          utpApi.preferences.saveResultsRepository(config.appvance.host, params, function(err){
            if(err){
              log.error("Failed to set Result Repository");
              log.error(err);
            }else{
              log.warn("Updated result repository");
            }
          });
        }
      });
    }
  });
}

function enableDevTools(){
  global.windows[0].webContents.openDevTools();
}

function setMenu(){
  var template = [];
  if (process.platform === 'darwin') {
    template.push({
      label: 'FromScratch',
      submenu: [{
        label: 'Quit',
        accelerator: 'CmdOrCtrl+Q',
        click: stopAllProcess
      }]
    });
  } else {
    template.push({
      label: 'File',
      submenu: [{
        label: 'Exit',
        accelerator: 'alt+f4',
        click: stopAllProcess
      }]
    });
  }
  template.push([{
    label: 'Edit',
    submenu: [{
      label: 'Undo',
      accelerator: 'CmdOrCtrl+Z',
      selector: 'undo:'
    }, {
      label: 'Redo',
      accelerator: 'Shift+CmdOrCtrl+Z',
      selector: 'redo:'
    }, {
      type: 'separator'
    }, {
      label: 'Cut',
      accelerator: 'CmdOrCtrl+X',
      selector: 'cut:'
    }, {
      label: 'Copy',
      accelerator: 'CmdOrCtrl+C',
      selector: 'copy:'
    }, {
      label: 'Paste',
      accelerator: 'CmdOrCtrl+V',
      selector: 'paste:'
    }, {
      label: 'Select All',
      accelerator: 'CmdOrCtrl+A',
      selector: 'selectAll:'
    }]
  }]);

  if (config.isDevEnv){
    var devTemplate = {
      label: 'Dev Tools',
      submenu: [{
        label: 'Enable DevTools',
        accelerator: 'Shift+CmdOrCtrl+I',
        click: enableDevTools
      },{
        label: "Download Logs",
        click: utils.downloadLogs
      }]
    };
    template.push(devTemplate);
  }
  log.debug("Setting menu options. "+JSON.stringify(template));
  var menu = Menu.buildFromTemplate(template);
  Menu.setApplicationMenu(menu);
}

function setTray(){
  log.debug("Setting Tray Icon");
  var tray = new Tray(config.trayImage);
  const contentMenu = Menu.buildFromTemplate([
    {label:"Open Appvance Window", click: startUpWindow},
    {label:"Shutdown Appvance", click: stopAllProcess}
  ]);
  tray.setToolTip('Start/Stop Appvance');
  tray.setContextMenu(contentMenu);
}

function startUpWindow(){
  app.dock.show();
  // Create the browser window.
  var options = {
    width: 1400,
    height: 1000,
    show: false
  };

  var startUpBrowserWindow = new BrowserWindow(options);
  global.windows[0] = startUpBrowserWindow;
  if (config.isDevEnv){
    startUpBrowserWindow.openDevTools();    
  }

  if (config.mysqlType === "default"){
    installMysql(function(err){
      if(err){
        log.error("Failed to install mysql");
        log.error(err);
      } else{
        startMysql();
        maintainDbConnection();
        provisionMysql(function(err){
          if (err){
            log.error("Failed to Provision Mysql");
            log.error(err);
          } else{
                     
          }
        });
      }
    });
  } else if(config.mysqlType === "cred"){
    maintainDbConnection();
    provisionMysql(function(err){
      if(err){
        log.error("Failed to Provision mysql");
        log.error(err);
      }
    });
  } 
  //else user provisions mysql on their own and we are trusting it is up and running
  startAppvance();
  log.info("Starting Appvance Electron");

  if (config.registered){
    startUpBrowserWindow.loadURL(`file://${__dirname}/init.html`);
  } else {
    startUpBrowserWindow.loadURL(`file://${__dirname}/signin.html`);
  }

  startUpBrowserWindow.on('ready-to-show', function() {
    startUpBrowserWindow.show();
    startUpBrowserWindow.focus();
    global.windows[0] = startUpBrowserWindow;
    startUpBrowserWindow = null;
  });


  // Open the DevTools.
  // global.windows[0].webContents.openDevTools()

  // Emitted when the window is closed.
  startUpBrowserWindow.on('closed', function() {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should devare the corresponding element.
    app.dock.hide();
    startUpBrowserWindow = null;
  });

  electron.ipcMain.on('register', (event, arg) => {
    acmConnector.registerClient(arg.companyName, arg.userName, (err, license) => {
      if (err) {
        log.error("Failure registering with acm");
        var message={
          success: false
        };
        if (err.code === "ENOTFOUND"){
          message.information = "Unable to resolve '"+err.hostname+"' Are you connected to the internet?";
        }
        startUpBrowserWindow.webContents.send('registerResult', message);
      } else {

        acmConnector.setLicense(license, function(err){
          if(err){
            log.error("Unable to set license");
            log.error(err);
            startUpBrowserWindow.webContents.send('registerResult', {
              success: false
            });
          } else{
            log.debug("Sending register message to client");
            startUpBrowserWindow.webContents.send('registerResult', {
              success: true
            });
            config.userName = arg.userName;
            config.companyName = arg.companyName;
            config.registered = true;
            config.saveSystemSettings();
            // startAppvance();
          }
        });
      }
    });
  });
}

function setupWindow(){
  var options = {
    width: 1400,
    height: 1000
  };

  var setupBrowserWindow = new BrowserWindow(options);
  if (config.isDevEnv){
    setupBrowserWindow.openDevTools();    
  }

  log.info("Starting Appvance Setup");
  setupBrowserWindow.loadURL(`file://${__dirname}/setup.html`);
  global.windows[0] = setupBrowserWindow;
  electron.ipcMain.on('install', function(event, arg){
    if (arg == "default"){
      startUpWindow();
    } else if(arg == "advanced"){
      advancedWindow();
    }
  });
}

function advancedWindow(){
  var options = {
    width: 1400,
    height: 1000,
    show: false
  };

  var advancedBrowserWindow = new BrowserWindow(options);
  if (config.isDevEnv){
    advancedBrowserWindow.openDevTools();    
  }

  log.info("Starting Appvance Setup");
  advancedBrowserWindow.loadURL(`file://${__dirname}/setupAdvanced.html`);

  advancedBrowserWindow.on('ready-to-show', function() {
    advancedBrowserWindow.show();
    advancedBrowserWindow.focus();
    global.windows[0] = advancedBrowserWindow;
    advancedBrowserWindow = null;
  });
  electron.ipcMain.on('setConfig', function(event, arg){
    config.mysqlType = arg.mysqlConfig.mysqlType;
    if (config.mysqlType === 'cred'){
      config.mysql.user = arg.mysqlConfig.user;
      config.mysql.password = arg.mysqlConfig.pass;
      config.mysql.host = arg.mysqlConfig.host;
      config.mysql.port = arg.mysqlConfig.port;
    } else if (config.mysqlType === 'self'){
      config.mysql.host = arg.mysqlConfig.host;
      config.mysql.port = arg.mysqlConfig.port;      
    }

    config.appvance.host = arg.appvanceConfig.host;
    config.appvance.port = arg.appvanceConfig.port;

    if (arg.appvanceSettings.user !== 'appvance'){
      utils.onceAppvanceUp(function(err){
        if(err){
          log.error(err);
        } else{
          utpApi.admin.loggin(config.appvance.host, config.appvance.user, config.appvance.password, function(err) {
            if (err) {
              log.error(err);
            } else {
              utpApi.admin.addUser(config.appvance.host, arg.appvanceConfig.user, arg.appvanceConfig.pass, 'admin', function(err){
                if(err){
                  log.error(err);
                } else{
                  config.appvance.user = arg.appvanceConfig.user;
                  config.appvance.password = arg.appvanceConfig.pass;
                }
              });
            }
          });
        }
      });
    } else if (arg.appvanceSettings.pass !== 'appvance'){
      utils.onceAppvanceUp(function(err){
        if(err){
          log.error(err);
        } else{
          utpApi.admin.loggin(config.appvance.host, config.appvance.user, config.appvance.password, function(err) {
            if (err) {
              log.error(err);
            } else {
              utpApi.admin.changePassword(config.appvance.host, arg.appvanceConfig.pass, function(err){
                if(err){
                  log.error(err);
                } else{
                  config.appvance.password = arg.appvanceConfig.password;
                }
              });
            }
          });
        }
      });
    }
    startUpWindow();
  });
}


function mainApplication(){
  setMenu();
  setTray();
  if (!config.mysqlInitialized){
    setupWindow();
  } else{
    startUpWindow();
  }
}


// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', mainApplication);

// Quit when all windows are closed.
app.on('window-all-closed', function() {
  // log.warn("shutting down");
  // stopAllProcess();
  // if (process.platform !== 'darwin') {
  //   app.quit();
  // }
  // app.quit();
});

function stopAllProcess() {
  stoppingProgram = true;
  if (mysqlChild) {
    log.debug("stopping mysql");
    mysqlChild.kill();

  }
  if (appvanceChild) {
    log.debug("stopping appvance service");
    appvanceChild.kill();

  }

  var checkKilled = function(){
    setTimeout(function(){
      if(appvanceChild.pid){
        log.error("Still haven't killed appvance");
        checkKilled();
      } else{
        log.error("Appvance killed");
        config.appvanceCommandStderrWriteSteam.end();
        config.appvanceCommandStdoutWriteSteam.end();
      }
      if(mysqlChild.pid){
        log.error("Still haven't killed mysql");
        checkKilled();
      } else{
        log.error("Mysql killed");
        config.mysqlCommandStdoutWriteSteam.end();
        config.mysqlCommandStderrWriteSteam.end();
      }
      app.quit();
    }, 5000);
  };
  // process.exit()
}

// app.on('activate', function() {
//   // On OS X it's common to re-create a window in the app when the
//   // dock icon is clicked and there are no other windows open.
//   if (global.windows[0] === null) {
//    startUpWindow();
//   }
// });


app.on('will-quit', function() {
  stopAllProcess();
});




// process.on('uncaughtException', function (error) {
//   log.error("Handling uncaught error")
//   if (global.windows[0]){
//     global.windows[0].webContents.send('current-proc', 'error setting up Appvance.')
//   } else{
//     console.log(error)
//   }
//   log.error(error)
//     // Handle the error
// })


//