'use strict';
var fs = require('fs');
const path = require('path');
var log = require('electron-log');
var uuid = require('node-uuid');

var config = {};

//Determine File locations and variable names by platform
var baseFilePath;
var javaExecutable;
var trayImage;
if (process.platform == 'win32') {
  javaExecutable = 'java.exe';
  baseFilePath = path.normalize('resources');
  trayImage = path.join(baseFilePath, 'icons','utp32.png');
} else if (process.platform == 'darwin') {
  javaExecutable = 'java';
  baseFilePath = path.resolve(path.join(__dirname, '../'));
  trayImage = path.join(baseFilePath, 'icons','utp16.png');
} else {
  throw new Error("cannot determine os " + process.platform);
}
config.baseFilePath = baseFilePath;
config.javaExecutable = javaExecutable;
config.trayImage = trayImage;

var templatePath = path.normalize(path.join(baseFilePath, 'Appvance','TestNode','webapps','Appvance','templates'));


//!!!!!!!!!!! Disable Registration for now!!!!!!!!!!!!
config.registered = true;

//////////////////////////////////////////////////
// Mysql properties
//////////////////////////////////////////////////
config.mysqlType = "default";
config.mysql = {
    host: 'localhost',
    user: 'root',
    password: 'SuperSecretPass',
    port: 54321
};
config.getDbConfig = function(){
  return {
    multipleStatements: true,
    host: config.mysql.host,
    user: config.mysql.user,
    password: config.mysql.password,
    port: config.mysql.port
  };
};

config.getMysqlInitCommand = function(){
  return path.normalize(path.join(baseFilePath, 'mysql', 'bin', 'mysqld')) +
    ' --initialize' +
    ' --basedir=' +
    path.normalize(path.join(baseFilePath, 'mysql')) +
    ' --datadir=' +
    path.normalize('mysql_data');
    // log.debug("mysqlInitCommand: " + config.mysqlInitCommand);
};

config.getMysqlStartCommand = function(){
  return path.normalize(path.join(baseFilePath, 'mysql', 'bin', 'mysqld')) +
    ' --basedir=' +
    path.normalize(path.join(baseFilePath, 'mysql')) +
    ' --datadir=' +
    path.normalize(path.join('mysql_data')) +
    ' --init-file=' +
    path.normalize(path.join('../', '../', 'mysql_init.sql')) +
    ' --log_syslog=0' +
    ' --port='+config.mysql.port +
    ' --bind-address='+config.mysql.host;
};
log.debug("mysqlStartCommand: " + config.mysqlStartCommand);
config.mysqlStartMainCommand = path.normalize(path.join(baseFilePath, 'mysql', 'bin', 'mysqld'));
config.getMysqlStartMainCommandArgs = function(){
  return [
    ' --basedir='+path.normalize(path.join(baseFilePath, 'mysql')),
    ' --datadir='+path.normalize(path.join('mysql_data')),
    ' --init-file='+path.normalize(path.join('../', '../', 'mysql_init.sql')),
    ' --general_log',
    ' --log_syslog=0',
    ' --port='+config.mysql.port,
    ' --bind-address='+config.mysql.host
  ];
};

config.mysqlCommandOutputPath = path.normalize(path.join(baseFilePath, 'mysql.stdout'));
config.mysqlCommandErrorPath = path.normalize(path.join(baseFilePath, 'mysql.stderr'));
config.mysqlCommandStdoutWriteSteam = fs.createWriteStream(config.mysqlCommandOutputPath);
config.mysqlCommandStderrWriteSteam = fs.createWriteStream(config.mysqlCommandErrorPath);

config.mysqlInitSqlFile = path.normalize(path.join(baseFilePath, 'Appvance', 'TestNode', 'webapps', 'AppvanceServer', 'WEB-INF', 'classes', 'database', 'MySQLInitial.sql'));
// log.debug("mysqlInitSqlFile: " + mysqlInitSqlFile);

config.mysqlAppvanceSqlFile = path.normalize(path.join(baseFilePath, 'Appvance', 'TestNode', 'webapps', 'AppvanceServer', 'WEB-INF', 'classes', 'database', 'MySQLAppvance.sql'));
// log.debug("mysqlAppvanceSqlFile: " + mysqlAppvanceSqlFile);
config.getMysqlAppvanceSqlCommand = function(){
  return path.normalize(path.join(baseFilePath, 'mysql', 'bin', 'mysql')) +
    ' -u'+config.mysql.user +
    ' -p'+config.mysql.password +
    ' -P'+config.mysql.port +
    ' -e "source ' +
    config.mysqlAppvanceSqlFile +
    '" ' +
    ' --verbose';
};

// Might be needed to further kill mysql
// config.mysqlStopCommand = path.normalize(path.join(baseFilePath, 'mysql', 'bin', 'mysqladmin')) +
//   ' -uroot' +
//   ' -pSuperSecretPass' +
//   ' -P54321' +
//   ' shutdown';
// log.debug("mysqlStopCommand: " + mysqlStopCommand);


//////////////////////////////////////////////////
// Appvance Java Service Properties
//////////////////////////////////////////////////
config.appvance = {};
config.appvance.port = 8080;
config.appvance.host = '0.0.0.0';
config.appvance.user = 'appvance';
config.appvance.password = 'appvance';
config.appvance.getUrl = function(){
  return 'http://'+config.appvance.host+':'+config.appvance.port+'/Appvance';
};


config.appvanceStartDir = path.normalize(path.join(baseFilePath, 'Appvance', 'TestNode'));
// log.debug("appvanceStartCommand: " + appvanceStartDir);
config.getAppvanceCommand = function(){
  return path.normalize(path.join('../', '../', 'jre', 'jre7', 'bin', javaExecutable)) +
    ' -cp "lib/etc/*"' +
    ' "-Djetty.logs=%UserProfile%/TM/Logs"' +
    ' -DSTOP.PORT=8079' +
    ' -Djetty.port=' + config.appvance.port +
    ' -Djetty.host=' + config.appvance.host +
    ' -DSTOP.KEY=secret' +
    ' -jar start.jar';
};

config.appvanceMainCommand = path.normalize(path.join('../', '../', 'jre', 'jre7', 'bin', javaExecutable));
config.getAppvanceMainCommandArgs = function(){
  return [
    ' -cp "lib/etc/*"',
    ' "-Djetty.logs=%UserProfile%/TM/Logs"',
    ' -DSTOP.PORT=8079',
    ' -Djetty.port=' + config.appvance.port,
    ' -Djetty.host=' + config.appvance.host,
    ' -DSTOP.KEY=secret',
    ' -jar start.jar'
  ];
};
config.appvanceCommandOutputPath = path.normalize(path.join(baseFilePath, 'appvance.stdout'));
config.appvanceCommandErrorPath = path.normalize(path.join(baseFilePath, 'appvance.stderr'));
config.appvanceCommandStdoutWriteSteam = fs.createWriteStream(config.appvanceCommandOutputPath);
config.appvanceCommandStderrWriteSteam = fs.createWriteStream(config.appvanceCommandErrorPath);

// Path to write license file to
config.licenseFile = path.normalize(path.join(baseFilePath, 'lic'));

//////////////////////////////////////////////////
//ACM properties
//////////////////////////////////////////////////
config.acm = {};
if (process.env.NODE_ENV === 'DEVELOPMENT'){
  config.acm.url = 'http://localhost:5000';
} else{
  config.acm.url = 'http://52.32.107.133:8888';
}
log.debug('ACM url: '+config.acm.url);

config.isDevEnv = false;
if (process.env.NODE_ENV === 'DEVELOPMENT'){
  config.isDevEnv = true;
  log.debug("Dev mode enabled");
}


//////////////////////////////////////////////////
// Writing settings to file properties
//////////////////////////////////////////////////
config.systemSettingsFile = config.licenseFile = path.normalize(path.join(baseFilePath, 'ssf'));

config.saveSystemSettings = function() {
  // log.debug("ssf: " + JSON.stringify(config));
  fs.writeFileSync(config.systemSettingsFile, JSON.stringify(config));
};

var readSystemSettings = function() {
  var readSettings = JSON.parse(fs.readFileSync(config.systemSettingsFile).toString());
  // log.debug("read settings: "+JSON.stringify(readSettings));
  if (!config) {
    config = readSettings;
    // log.debug("fresh config is "+JSON.stringify(config.systemSettings));
  } else {
    // log.debug("config already is "+JSON.stringify(config.systemSettings));
    config = extend(readSettings, config);
    // log.debug("merged config is "+JSON.stringify(config.systemSettings));
  }
};


//////////////////////////////////////////////////
// Start Up Checks
//////////////////////////////////////////////////
function editTestNodeUrl(file){
  fs.readFile(file, 'utf8', function (err,data) {
    if (err) {
      log.error(err);
    } else{
      // log.info('updateing file '+file);

      var result = data.replace(/http:\/\/localhost:8080\/TMServer\/ws\/TestNode/g, 'http://localhost:'+config.appvance.port+'/TMServer/ws/TestNode');

      fs.writeFile(file, result, 'utf8', function (err) {
         if (err) {return console.log(err);}
      });
    }
  });
}

function startUpChecks() {
  if (!('uuid' in config)) {
    log.warn("uuid not in config, new instance of appvance.");
    config.uuid = uuid.v4();
    config.saveSystemSettings();
  }

  fs.readdir(templatePath, function(err, files){
    log.info("editing template files");
    for (var i=0; i<files.length;i++){
      let fileToEdit = path.normalize(path.join(templatePath, files[i]));
      editTestNodeUrl(fileToEdit);
    }
  });

}

//Startup Code
try {
  fs.statSync(config.systemSettingsFile).isFile();
  readSystemSettings();
  log.debug("Loading system settings: " + JSON.stringify(config));
} catch (err) {
  if (err.code !== "ENOENT"){
    log.error(err);
  }
  fs.writeFileSync(config.systemSettingsFile, "{}");
}

function extend(obj, src) {
  for (var key in src) {
    if (src.hasOwnProperty(key)) {
      obj[key] = src[key];
    }
  }
  return obj;
}

module.exports = config;
startUpChecks();