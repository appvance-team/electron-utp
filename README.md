# README #

In order for this wrapper to work, UTP must be unzipped into the root folder, i.e. Appvance.sh and all others must be at './' along with this repository. For the sake of portability, a UTP install has been omitted.

## Install

1. Unzip utp3.0.zip into root directory (./)
2. $ npm install
3. $ npm start
4. Potentially reload the Electron window once the java processes startup entirely. It may startup blank initially.
